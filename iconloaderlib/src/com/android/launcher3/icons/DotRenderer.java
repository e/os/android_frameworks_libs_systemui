/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.launcher3.icons;

import static android.graphics.Paint.ANTI_ALIAS_FLAG;
import static android.graphics.Paint.FILTER_BITMAP_FLAG;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.Log;
import android.view.ViewDebug;

import androidx.core.graphics.ColorUtils;

/**
 * Used to draw a notification dot on top of an icon.
 */
public class DotRenderer {

    private static final String TAG = "DotRenderer";

    // The dot size is defined as a percentage of the app icon size.
    private static final float SIZE_PERCENTAGE = 0.21f;
    private static final float SIZE_PERCENTAGE_WITH_COUNT = 0.28f;
    private static final float SIZE_PERCENTAGE_WITH_COUNT_TABLET = 0.24f;

    // The max number to draw on dots
    private static final int MAX_COUNT = 999;

    private final float mCircleRadius;
    private final Paint mCirclePaint = new Paint(ANTI_ALIAS_FLAG | FILTER_BITMAP_FLAG);
    private final Paint mCircleShadowPaint = new Paint(ANTI_ALIAS_FLAG | FILTER_BITMAP_FLAG);

    private final Paint mTextPaint = new Paint(ANTI_ALIAS_FLAG | FILTER_BITMAP_FLAG);

    private final Bitmap mBackgroundWithShadow;
    private final float mBitmapOffset;

    // Stores the center x and y position as a percentage (0 to 1) of the icon size
    private final float[] mRightDotPosition;
    private final float[] mLeftDotPosition;

    private static final int MIN_DOT_SIZE = 1;
    private final Rect mTextRect = new Rect();
    private final boolean mDisplayCount;

    public DotRenderer(int iconSizePx, Path iconShapePath, int pathSize) {
        this(iconSizePx, iconShapePath, pathSize, false, null, false);
    }

    public DotRenderer(int iconSizePx, Path iconShapePath, int pathSize, Boolean displayCount, Typeface typeface, boolean isTablet) {
        mDisplayCount = displayCount;
        int size = Math.round((displayCount ? (isTablet ? SIZE_PERCENTAGE_WITH_COUNT_TABLET : SIZE_PERCENTAGE_WITH_COUNT) : SIZE_PERCENTAGE) * iconSizePx);
        if (size <= 0) {
            size = MIN_DOT_SIZE;
        }
        ShadowGenerator.Builder builder = new ShadowGenerator.Builder(Color.TRANSPARENT);
        builder.ambientShadowAlpha = 88;
        mBackgroundWithShadow = builder.setupBlurForSize(size).createPill(size, displayCount ? (size - 25) : size);
        mCircleRadius = builder.radius;

        mBitmapOffset = -mBackgroundWithShadow.getHeight() * 0.5f; // Same as width.

        // Find the points on the path that are closest to the top left and right corners.
        mLeftDotPosition = getPathPoint(iconShapePath, pathSize, -1);
        mRightDotPosition = getPathPoint(iconShapePath, pathSize, 1);

        mTextPaint.setTextSize(size * 0.65f);
        mTextPaint.setTextAlign(Paint.Align.LEFT);
        mTextPaint.setTypeface(typeface);
    }

    private static float[] getPathPoint(Path path, float size, float direction) {
        float halfSize = size / 2;
        // Small delta so that we don't get a zero size triangle
        float delta = 1;

        float x = halfSize + direction * halfSize;
        Path trianglePath = new Path();
        trianglePath.moveTo(halfSize, halfSize);
        trianglePath.lineTo(x + delta * direction, 0);
        trianglePath.lineTo(x, -delta);
        trianglePath.close();

        trianglePath.op(path, Path.Op.INTERSECT);
        float[] pos = new float[2];
        new PathMeasure(trianglePath, false).getPosTan(0, pos, null);

        pos[0] = pos[0] / size;
        pos[1] = pos[1] / size;
        return pos;
    }

    public float[] getLeftDotPosition() {
        return mLeftDotPosition;
    }

    public float[] getRightDotPosition() {
        return mRightDotPosition;
    }

    /**
     * Draw a circle on top of the canvas according to the given params.
     */
    public void draw(Canvas canvas, DrawParams params) {
        draw(canvas, params, 0);
    }

    public void draw(Canvas canvas, DrawParams params, int numNotifications) {
        draw(canvas, params, numNotifications, false, false);
    }

    public void draw(Canvas canvas, DrawParams params, int numNotifications,
                     boolean isTablet, boolean isTaskbar) {
        if (params == null) {
            Log.e(TAG, "Invalid null argument(s) passed in call to draw.");
            return;
        }
        canvas.save();

        Rect iconBounds = params.iconBounds;
        float[] dotPosition = params.leftAlign ? mLeftDotPosition : mRightDotPosition;
        float dotCenterX = iconBounds.left + iconBounds.width() * dotPosition[0];
        float dotCenterY = iconBounds.top + iconBounds.height() * dotPosition[1];

        // Ensure dot fits entirely in canvas clip bounds.
        Rect canvasBounds = canvas.getClipBounds();

        if (isTablet) {
            // We draw the dot relative to its center.
            float availableWidth = (float) (canvasBounds.width() - iconBounds.width()) / 2;

            // We check if the dot is too close to the edge of the screen and nudge it if necessary.
            boolean mustNudge = availableWidth < mCircleRadius;
            float dx = mustNudge ? mCircleRadius : dotCenterX;

            // Taskbar icons are too small and do not show the dot text.
            // We will displace the dot to the correct position.
            if (isTaskbar) {
                canvas.translate(dotCenterX - mCircleRadius, dotCenterY - mCircleRadius);
            } else if (numNotifications > 9 && numNotifications < 1000) {
                canvas.translate(dx - (mustNudge ? 12f : 17f), dotCenterY);
            } else if (numNotifications > 0) {
                canvas.translate(dx - 12f, dotCenterY);
            }
        } else {
            float offsetX = params.leftAlign
                    ? Math.max(0, canvasBounds.left - (dotCenterX + mBitmapOffset))
                    : Math.min(0, canvasBounds.right - (dotCenterX - mBitmapOffset));
            float offsetY = Math.max(0, canvasBounds.top - (dotCenterY + mBitmapOffset));

            // We draw the dot relative to its center.
            float dx = dotCenterX + offsetX;
            float dy = dotCenterY + offsetY - 15f;

            if (numNotifications > 9 && numNotifications < 1000) {
                canvas.translate(dx - 17f, dy);
            } else if (numNotifications > 0) {
                canvas.translate(dx - 12f, dy);
            }
        }

        float scale = params.scale * (isTablet && !isTaskbar ? 0.75f : 1f);
        canvas.scale(scale, scale);

        mCirclePaint.setColor(params.dotColor);
        mCircleShadowPaint.setColor(params.shadowDotColor);

        if (!isTaskbar && numNotifications >= 10 && numNotifications < 1000) {
            canvas.drawRoundRect(new RectF(-mCircleRadius + 10, -mCircleRadius, mCircleRadius + 20, mCircleRadius), 50, 50, mCircleShadowPaint);
            canvas.drawRoundRect(new RectF(-mCircleRadius + 10, -mCircleRadius, mCircleRadius + 20, mCircleRadius), 50, 50, mCirclePaint);
        } else if (numNotifications > 0) {
            canvas.drawCircle(5, 10, mCircleRadius, mCircleShadowPaint);
            canvas.drawCircle(5, 10, mCircleRadius, mCirclePaint);
        }

        if (mDisplayCount && numNotifications > 0 && !isTaskbar) {
            // Draw the numNotifications text
            mTextPaint.setColor(getCounterTextColor(Color.WHITE));
            mTextPaint.setTypeface(Typeface.DEFAULT_BOLD);
            mTextPaint.setTextSize(32f);
            String text = numToNotation(numNotifications);
            mTextPaint.getTextBounds(text, 0, text.length(), mTextRect);
            float y = mTextRect.height() / 2f - mTextRect.bottom;
            if (numNotifications < 10) {
                canvas.drawText(text, -4f, 22f, mTextPaint);
            } else if (numNotifications < 100) {
                canvas.drawText(text, -3f, y, mTextPaint);
            } else if (numNotifications >= 1000) {
                canvas.drawText(text, -14f, 20f, mTextPaint);
            } else {
                canvas.drawText(text, -12f, y, mTextPaint);
            }
        }

        canvas.restore();
    }

    private String numToNotation(int num) {
        if (num < 1000) {
            return String.valueOf(num);
        } else {
            return num / 1000 + "k";
        }
    }

    /**
     * Returns the color to use for the counter text based on the dot's background color.
     *
     * @param dotBackgroundColor The color of the dot background.
     * @return The color to use on the counter text.
     */
    private int getCounterTextColor(int dotBackgroundColor) {
        return ColorUtils.setAlphaComponent(dotBackgroundColor, 0xFF);
    }

    public static class DrawParams {
        /** The color (possibly based on the icon) to use for the dot. */
        @ViewDebug.ExportedProperty(category = "notification dot", formatToHexString = true)
        public int dotColor;
        /** The color (possibly based on the icon) to use for a predicted app. */
        @ViewDebug.ExportedProperty(category = "notification dot", formatToHexString = true)
        public int appColor;
        /** The bounds of the icon that the dot is drawn on top of. */
        @ViewDebug.ExportedProperty(category = "notification dot")
        public Rect iconBounds = new Rect();
        /** The progress of the animation, from 0 to 1. */
        @ViewDebug.ExportedProperty(category = "notification dot")
        public float scale;
        /** Whether the dot should align to the top left of the icon rather than the top right. */
        @ViewDebug.ExportedProperty(category = "notification dot")
        public boolean leftAlign;
        @ViewDebug.ExportedProperty(category = "notification dot", formatToHexString = true)
        public int shadowDotColor;
    }
}
